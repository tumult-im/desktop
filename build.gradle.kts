import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
	mavenCentral()
}

plugins {
	kotlin("jvm") version "1.6.10"
	kotlin("plugin.serialization") version "1.6.10"
	application
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "17"

kotlin {
	sourceSets {
		dependencies {
			// AGPL 3.0:
			implementation("net.folivo:trixnity-client:1.1.5")
			implementation("net.folivo:trixnity-client-store-exposed:1.1.5")

			// Apache 2.0:
			implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0")
			implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:1.6.0")
			implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
			implementation("io.ktor:ktor-client-java:1.6.7")
			implementation("com.formdev:flatlaf:2.0-rc1")
			implementation("org.jetbrains.exposed:exposed-core:0.37.3")

			// MPL 2.0 or EPL 1.0:
			implementation("com.h2database:h2:2.0.206")

			// MIT:
			implementation("org.slf4j:slf4j-jdk14:2.0.0-alpha5")
		}
	}
}

application {
	applicationDefaultJvmArgs = listOf("-ea", "-Djna.library.path=${System.getProperty("jna.library.path")?:""}")
	mainClass.set("im.tumult.desktop.MainKt")
}
