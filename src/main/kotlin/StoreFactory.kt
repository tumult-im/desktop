package im.tumult.desktop

// TODO: this is copied from the trixnity examples. db should be stored in a ~/.local/share/something kind of dir

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

import net.folivo.trixnity.client.store.StoreFactory
import net.folivo.trixnity.client.store.exposed.ExposedStoreFactory

import org.jetbrains.exposed.sql.Database

fun createStoreFactory(): StoreFactory {
	return ExposedStoreFactory(
		database = Database.connect("jdbc:h2:./im.tumult.desktop.db;DB_CLOSE_DELAY=-1;"),
		transactionDispatcher = Dispatchers.IO,
		scope = CoroutineScope(Dispatchers.Default),
	)
}
