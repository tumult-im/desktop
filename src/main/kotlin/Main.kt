package im.tumult.desktop

import io.ktor.http.*

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.swing.Swing

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName
import kotlinx.serialization.json.Json

import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.getStateKey
import net.folivo.trixnity.client.room.*
import net.folivo.trixnity.client.store.StoreFactory
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.store.TimelineEvent.Gap.GapBefore
import net.folivo.trixnity.client.api.model.authentication.IdentifierType.User
import net.folivo.trixnity.core.model.RoomId

import net.folivo.trixnity.core.model.events.Event
import net.folivo.trixnity.core.model.events.StateEventContent
import net.folivo.trixnity.core.model.events.EphemeralEventContent
import net.folivo.trixnity.core.model.events.GlobalAccountDataEventContent
import net.folivo.trixnity.core.model.events.MessageEventContent
import net.folivo.trixnity.core.model.events.RoomAccountDataEventContent
import net.folivo.trixnity.core.model.events.ToDeviceEventContent
import net.folivo.trixnity.core.serialization.events.EventContentSerializerMappings
import net.folivo.trixnity.core.serialization.events.EventContentSerializerMapping

import javax.swing.JPanel
import javax.swing.JFrame
import javax.swing.BoxLayout
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension

class MainFrame() {
	val frame = JFrame(trans("Tumult Desktop"))
	init {
		frame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
		frame.setBounds(0, 0, 1280, 720)
	}

	val mainPanel = JPanel(null)
	init {
		mainPanel.layout = BoxLayout(mainPanel, BoxLayout.PAGE_AXIS)
		mainPanel.background = Color(0xcf6902)
		frame.add(mainPanel, BorderLayout.CENTER)
	}

	val topPanel = JPanel(null)
	val bottomPanel = JPanel(null)

	init {
		topPanel.layout = BoxLayout(topPanel, BoxLayout.LINE_AXIS)
		bottomPanel.layout = BoxLayout(bottomPanel, BoxLayout.LINE_AXIS)

		topPanel.background = Color(0xff00ff)
		topPanel.preferredSize = Dimension(1280, 60)
		bottomPanel.background = Color(0x00ffff)
		bottomPanel.preferredSize = Dimension(1280, 648)

		mainPanel.add(topPanel)
		mainPanel.add(bottomPanel)
	}

	val identities = IdentitiesPanel()
	val roomInfo = RoomInfoPanel()
	val roomsContainer = RoomsContainerPanel()
	val currentRoom = ChatRoomPanel()

	val roomGroups = RoomGroupsPanel()
	val chatRooms = ChatRoomsPanel()

	init {
		identities.preferredSize = Dimension(160, 60)
		roomInfo.preferredSize = Dimension(1120, 60)
		roomsContainer.preferredSize = Dimension(160, 660)
		currentRoom.preferredSize = Dimension(1120, 660)

		topPanel.add(identities)
		topPanel.add(roomInfo)
		bottomPanel.add(roomsContainer)
		bottomPanel.add(currentRoom)
	}

	fun show() {
		//frame.pack()
		frame.setLocationRelativeTo(null)
		frame.setVisible(true)
	}
}

class IdentitiesPanel : JPanel() {
	init {
		this.background = Color(0xff0000)
	}
}

class RoomInfoPanel : JPanel() {
	init {
		this.background = Color(0x00ff00)
	}
}

class RoomsContainerPanel : JPanel(null) {
	val childGroups = RoomGroupsPanel()
	val childRooms = ChatRoomsPanel()
	init {
		this.layout = BoxLayout(this, BoxLayout.LINE_AXIS)
		this.add(childGroups)
		this.add(childRooms)
	}
}

class RoomGroupsPanel : JPanel() {
	init {
		this.background = Color(0xffffff)
	}
}

class ChatRoomsPanel : JPanel() {
	init {
		this.background = Color(0x000000)
	}
}

class ChatRoomPanel : JPanel() {
	init {
		this.background = Color(0xffff00)
	}
}

fun main() = runBlocking {
	println("hello world :D")

	com.formdev.flatlaf.FlatLightLaf.setup()
	
	launch(Dispatchers.Swing) {
		println("test 1!")
		val mf = MainFrame()
		mf.show()

		println("test 2!")
	}
	println("swing dispatched!")
	return@runBlocking

	/*
	val scope = CoroutineScope(Dispatchers.Default)

	val storeFactory = createStoreFactory()

	val matrixClient = getMatrixClient(storeFactory)

	if (matrixClient == null) {
		println("couldn't load or create matrix client")
		return@runBlocking
	}

	println("see u soon ;)")

	matrixClient.api.sync.subscribe<SpaceChildEventContent> { spaceChildEv ->
		require(spaceChildEv is Event.StateEvent)
		println("spaceChildEv: ${spaceChildEv}")
	}
	/*
	matrixClient.api.sync.subscribe<CreateWithTypeEventContent> { createEv ->
		//println(it)
		require(createEv is Event.StateEvent)
		if (createEv.content.type == "m.space") {
			println("api.sync.subscribe<CreateWithTypeEventContent>: space: ${createEv.roomId}")
			/*
			matrixClient.room.getLastTimelineEvent(createEv.roomId, scope).filterNotNull().take(1).collect { lastEv ->
				flow {
					var currentEv: StateFlow<TimelineEvent?>? = lastEv
					emit(lastEv)
					while (currentEv?.value != null) {
						val evValue = currentEv.value
						if (evValue?.gap is GapBefore) {
							matrixClient.room.fetchMissingEvents(evValue)
						}
						currentEv = currentEv.value?.let {
							matrixClient.room.getPreviousTimelineEvent(it, scope)
						}
						emit(currentEv)
					}
				}.filterNotNull().take(100).toList().forEach { tlEv ->
					//println(tlEv.value?.event)
					val ev = tlEv.value?.event!!
					val content = ev.content
					if (content is SpaceChildEventContent) {
						println("  child: ${ev.getStateKey()}")
					}
				}
			}
			*/
		}
	}
	*/

	matrixClient.startSync()
	//delay(10000)

	//val roomId = RoomId("!NeXnkHDpZzjKJJCDGA:enby.zone")

	//println("m.room.create")
	//println(CreateWithTypeEventContent::class)
	//println(matrixClient.api.rooms.getStateEvent<CreateWithTypeEventContent>(roomId, ""))
	//println(matrixClient.room.getState<CreateWithTypeEventContent>(roomId, ""))

	println("\nrooms")
	matrixClient.room.getAll().collect { roomMap ->
		/* StateFlow<Map<RoomId, StateFlow<Room?>>> */
		//println("RoomService.getAll() stateflow update")
		for ((roomId, room) in roomMap) {
			//room.
			val roomCreate = matrixClient.room.getState<CreateWithTypeEventContent>(roomId, "", scope)
			scope.launch {
				roomCreate.filterNotNull().collect {
					if (it.content.type == "m.space") {
						//println("space ${roomId}: ${room.value}")
						println("space ${roomId}: ${room.value?.name?.explicitName}")
						matrixClient.room.getState<SpaceChildEventContent>(roomId, "", scope).filterNotNull().collect {
							println("${roomId} space child update: ${it}")
						}
					}
				}
			}
			/*
			if (roomCreate.value == null) {
				println("RoomService.getAll: oh god")
				println("  room: ${room.value}")
				scope.launch {
					room.collect {
						println("roomService.getAll()[]|room.collect: ${roomId} update: ${it}")
					}
				}
				return@collect
			}
			*/
			/*
			if (roomCreate.value != null && roomCreate.value.content.type == "m.space") {
				println("RoomService.getAll: space: '${room.value?.name?.explicitName}' (${roomId})")
			}
			*/
		}
	}

	println("we're done here.")
	matrixClient.stopSync()
	*/
}

suspend fun getMatrixClient(storeFactory: StoreFactory): MatrixClient? {
	val scope = CoroutineScope(Dispatchers.Default)

	val client = MatrixClient.fromStore(
		storeFactory = storeFactory,
		scope = scope,
		customMappings = CustomMappings,
	).getOrThrow()
	if (client != null) { return client }

	val credentials = LoginForm().getData()
	if (credentials == null) {
		println("user closed login form")
		return null
	}
	// TODO: validate username field on login form
	val afterAt = credentials.username.split("@", limit = 2)[1]
	val split = afterAt.split(":")
	val username = split[0]
	val baseUrl = "https://${split[1]}"

	println("logging '${username}' in at '${baseUrl}'...")

	// TODO: any way to avoid copy to String?
	// or to zero out a String's backing memory?
	val password = credentials.password.joinToString("")

	return MatrixClient.login(
		baseUrl = Url(baseUrl),
		identifier = User(username),
		password = password,
		initialDeviceDisplayName = "Tumult.im Desktop (Pre-alpha)",
		storeFactory = storeFactory,
		scope = scope,
		customMappings = CustomMappings,
	).getOrThrow()
}

object CustomMappings : EventContentSerializerMappings {
	override val ephemeral: Set<EventContentSerializerMapping<out EphemeralEventContent>> = setOf()
	override val globalAccountData: Set<EventContentSerializerMapping<out GlobalAccountDataEventContent>> = setOf()
	override val message: Set<EventContentSerializerMapping<out MessageEventContent>> = setOf()
	override val roomAccountData: Set<EventContentSerializerMapping<out RoomAccountDataEventContent>> = setOf()
	override val toDevice: Set<EventContentSerializerMapping<out ToDeviceEventContent>> = setOf()
	override val state: Set<EventContentSerializerMapping<out StateEventContent>> = setOf(
		EventContentSerializerMapping.Companion.of<CreateWithTypeEventContent>("m.room.create"),
		EventContentSerializerMapping.Companion.of<SpaceChildEventContent>("m.space.child"),
		EventContentSerializerMapping.Companion.of<JoinRulesRestrictedEventContent>("m.room.join_rules"),
	)
}

/** @see <a href="https://spec.matrix.org/v1.2/client-server-api/#mroomcreate">matrix spec</a> */
@Serializable
data class CreateWithTypeEventContent(
	@SerialName("creator")
	val creator: net.folivo.trixnity.core.model.UserId,
	@SerialName("m.federate")
	val federate: Boolean = true,
	@SerialName("room_version")
	val roomVersion: String = "1",
	@SerialName("type")
	val type: String? = null,
	@SerialName("predecessor")
	val predecessor: PreviousRoom? = null
) : StateEventContent {
	@Serializable
	data class PreviousRoom(
		@SerialName("room_id")
		val roomId: net.folivo.trixnity.core.model.RoomId,
		@SerialName("event_id")
		val eventId: net.folivo.trixnity.core.model.EventId
	)
}

@Serializable
data class SpaceChildEventContent(
	@SerialName("via")
	val via: List<String>? = null,
	@SerialName("suggested")
	val suggested: Boolean? = false,
	@SerialName("order")
	val order: String? = null,
) : StateEventContent

@Serializable
data class JoinRulesRestrictedEventContent(
	@SerialName("join_rule")
	val joinRule: JoinRule
) : StateEventContent {
	@Serializable
	enum class JoinRule {
		@SerialName("public")
		PUBLIC,
		@SerialName("knock")
		KNOCK,
		@SerialName("invite")
		INVITE,
		@SerialName("private")
		PRIVATE,
		@SerialName("restricted")
		RESTRICTED,
	}
}
