package im.tumult.desktop

import javax.swing.JFrame
import javax.swing.JComponent
import javax.swing.JOptionPane

import java.awt.event.WindowListener
import java.awt.event.ActionEvent
import java.awt.event.WindowEvent
import java.awt.GridBagConstraints

//import kotlinx.coroutines.channels.Channel
//import kotlinx.coroutines.channels.ChannelResult
//import kotlinx.coroutines.channels.trySendBlocking
//import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
//import kotlinx.coroutines.runBlocking

data class LoginData(val username: String, val password: CharArray)

class LoginForm : WindowListener {
	// base ui
	val frame = JFrame(trans("Login"))
	val layout = java.awt.GridBagLayout()
	val panel = javax.swing.JPanel(layout)
	init { frame.setContentPane(panel) }

	// active controls
	var usernameTextField = javax.swing.JTextField()
	var passwordField = javax.swing.JPasswordField()
	val button = javax.swing.JButton(trans("Login"))
	init {
		usernameTextField.setToolTipText("Username")
		passwordField.setToolTipText("Password")
		button.setToolTipText("Login")
	}

	// passive controls
	val usernameLabel = javax.swing.JLabel("Username")
	val passwordLabel = javax.swing.JLabel("Password")
	init {
		usernameLabel.setLabelFor(usernameTextField)
		passwordLabel.setLabelFor(passwordField)
	}

	// listen to this to receive username + password, or null if cancelled
	val dataChannel = Channel<LoginData>();

	// constraints to be used with controls
	var constraints = GridBagConstraints()
	// add some padding on every element
	init { constraints.insets = java.awt.Insets(5, 5, 5, 5) }

	var componentY = 0

	// constraint helper functions

	// constrain a component to the first column and return it
	fun col1(component: JComponent): JComponent {
		// this column is of a fixed size
		constraints.fill = GridBagConstraints.NONE
		constraints.gridx = 0
		constraints.gridy = componentY
		constraints.gridwidth = 1
		constraints.weightx = 0.0
		layout.setConstraints(component, constraints)
		return component
	}

	// constrain a component to the second column and return it
	fun col2(component: javax.swing.JComponent): JComponent {
		// this column expands to match the size of the window
		// it occupies the logical columns 2 and 3
		constraints.fill = GridBagConstraints.HORIZONTAL
		constraints.gridx = 1
		constraints.gridy = componentY
		constraints.gridwidth = 2
		constraints.weightx = 1.0
		layout.setConstraints(component, constraints)
		return component
	}

	// write to data channel if username/pass are valid
	fun validate() {
		if (usernameTextField.getText().length == 0) {
			JOptionPane.showMessageDialog(frame, "Username cannot be blank!", "Login error", JOptionPane.ERROR_MESSAGE)
			usernameTextField.requestFocusInWindow()
			return
		}
		if (passwordField.getPassword().size == 0) {
			JOptionPane.showMessageDialog(frame, "Password cannot be blank!", "Login error", JOptionPane.ERROR_MESSAGE)
			passwordField.requestFocusInWindow()
			return
		}

		button.setEnabled(false)
		val username = usernameTextField.getText()
		val password = passwordField.getPassword()
		val result = dataChannel.trySendBlocking(LoginData(username, password))
		result.onSuccess {
			dataChannel.close()
			frame.dispose()
		}
		.onFailure {
			button.setEnabled(true)
		}
	}

	init {
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)
		frame.addWindowListener(this)

		// layout

		panel.add(col1(usernameLabel))
		panel.add(col2(usernameTextField))
		componentY++

		panel.add(col1(passwordLabel))
		panel.add(col2(passwordField))
		componentY++

		constraints.fill = GridBagConstraints.NONE
		constraints.gridx = 1
		constraints.gridy = componentY
		constraints.gridwidth = 1
		constraints.weightx = 0.0
		layout.setConstraints(button, constraints)
		panel.add(button)

		usernameTextField.addActionListener { validate() }
		passwordField.addActionListener { validate() }
		button.addActionListener { validate() }

		frame.pack()
		frame.setBounds(0, 0, frame.getWidth() * 4 / 3 - 10, frame.getHeight())
		frame.setLocationRelativeTo(null)
		frame.setVisible(true)
	}

	// awaits either window close or form submit
	suspend fun getData(): LoginData? {
		return dataChannel.receiveCatching().getOrNull()
	}

	// close the data channel when the form closes
	override fun windowClosing(ev: WindowEvent) {
		runBlocking {
			dataChannel.close()
		}
		frame.dispose()
	}

	// several empty handlers to satisfy WindowListener
	override fun windowOpened(ev: WindowEvent) {}
	override fun windowClosed(ev: WindowEvent) {}
	override fun windowIconified(ev: WindowEvent) {}
	override fun windowDeiconified(ev: WindowEvent) {}
	override fun windowActivated(ev: WindowEvent) {}
	override fun windowDeactivated(ev: WindowEvent) {}
}
